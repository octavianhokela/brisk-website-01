import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from "../views/Home";
import Studies from "../views/Studies";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: 'Home',
        component: Home,
    },
    {
        path: "/studies",
        name: 'Studies',
        component: Studies,
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

export default router
